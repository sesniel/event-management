<div class="col-md-6">

        <?= $this->Form->create(null, ['class' => 'form-horizontal']); ?>
        <div class="panel panel-flat">
            <div class="panel-heading">
                    <h5 class="panel-title">User Registration<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                    <div class="heading-elements">
                            <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                            </ul>
                    </div>
            </div>

            <div class="panel-body">
                    
                    <div class="form-group">
                            <label class="control-label col-lg-2">Type</label>
                            <div class="col-lg-10">
                                <select name="type" class="form-control">
                                    <option value="client">Client</option>
                                    <option value="admin">Admin</option>
                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="control-label col-lg-2">First Name</label>
                            <div class="col-lg-10">
                                    <input class="form-control" type="text" name="first_name">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-lg-2">Last Name</label>
                            <div class="col-lg-10">
                                    <input class="form-control" type="text" name="last_name">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-lg-2">Position</label>
                            <div class="col-lg-10">
                                    <input class="form-control" type="text" name="position">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-lg-2">Gender</label>
                            <div class="col-lg-10">
                                <select name="theme" class="form-control">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="control-label col-lg-2">Email</label>
                            <div class="col-lg-10">
                                    <input class="form-control" type="email" name="email">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-lg-2">Password</label>
                            <div class="col-lg-10">
                                    <input class="form-control" type="password" name="password">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-lg-2">Theme</label>
                            <div class="col-lg-10">
                                <select name="theme" class="form-control">
                                    <option value="theme1">default</option>
<!--                                    <option value="theme2">Theme 2</option>-->
                                </select>
                        </div>
                    </div>

                    <div class="form-group">
                            <label class="control-label col-lg-2">Comment</label>
                            <div class="col-lg-10">
                                    <textarea rows="5" cols="5" class="form-control" name="comment" placeholder="Comment"></textarea>
                            </div>
                    </div>

                    <div class="text-right">
                            <button type="submit" class="btn btn-primary legitRipple">Register <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <?= $this->Form->end(); ?>

</div>

<div class="col-md-6">
    <div class="panel panel-flat">
            <div class="panel-heading">
                    <h5 class="panel-title">Registered User<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                    <div class="heading-elements">
                            <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
            </ul>
    </div>
            </div>

            <div class="table-responsive">
                    <table class="table">
                            <thead>
                                    <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                    </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach($userDetails as $user):
                                        echo '<tr>
                                                    <td>'.$user->id.'</td>
                                                    <td>'.$user->type.'</td>
                                                    <td>'.$user->first_name .'</td>
                                                    <td>'.$user->last_name.'</td>
                                                    <td>'.$user->email.'</td>
                                            </tr>';
                                    endforeach;
                                ?>
                            </tbody>
                    </table>
            </div>
    </div>
</div>