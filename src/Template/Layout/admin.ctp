<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $this->request->params['controller'] ?></title>

        <!-- Global stylesheets -->
	<?= $this->Html->css('icons/icomoon/styles.css') ?>
	<?= $this->Html->css('icons/icomoon/styles.css') ?>
	<?= $this->Html->css('bootstrap.css') ?>
	<?= $this->Html->css('core.css') ?>
	<?= $this->Html->css('components.css') ?>
	<?= $this->Html->css('colors.css') ?>
	<!-- /global stylesheets -->

</head>

<body >
<?php // debug($this->request); ?>
<!-- Page container -->
<?= $this->element('Nav/admin_main') ?>
<div class="page-container">
    <!-- Page content -->
        
    <div class="page-content">
        <?= $this->element('Nav/admin_sidebar') ?>

        <div class="content-wrapper">

            <?php echo $this->element('Nav/admin_content_header') ?>
            <div class="content">

                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
                <?= $this->element('Nav/admin_footer') ?>

            </div>

        </div>
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

    <!-- Core JS files -->
    <?= $this->Html->script('plugins/loaders/pace.min.js'); ?>
    <?= $this->Html->script('core/libraries/jquery.min.js'); ?>
    <?= $this->Html->script('core/libraries/bootstrap.min.js'); ?>
    <?= $this->Html->script('plugins/loaders/blockui.min.js'); ?>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <?= $this->Html->script('plugins/visualization/d3/d3.min.js'); ?>
    <?= $this->Html->script('plugins/visualization/d3/d3_tooltip.js'); ?>
    <?= $this->Html->script('plugins/forms/styling/switchery.min.js'); ?>
    <?= $this->Html->script('plugins/forms/styling/uniform.min.js'); ?>
    <?= $this->Html->script('plugins/forms/selects/bootstrap_multiselect.js'); ?>
    <?= $this->Html->script('plugins/ui/moment/moment.min.js'); ?>
    <?= $this->Html->script('plugins/pickers/daterangepicker.js'); ?>

    <?= $this->Html->script('core/app.js'); ?>
    <?= $this->Html->script('pages/dashboard.js'); ?>

    <?= $this->Html->script('plugins/ui/ripple.min.js'); ?>

</body>
</html>
