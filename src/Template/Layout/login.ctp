<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Tutoring Platform"/>
    <meta name="keywords" content="Online Assessment, Test Prep, Tutors, Review Center"/>
    <meta name="author" content="Tutor2You"/>
    <link rel="shortcut icon" href="/img/icon.png">
    <title>Login</title>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
</head>

<body>

<div class="container">
    <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <a class="dropdown-item" href="#">Something else here</a>
    </div>
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

    <footer class="footer">
        
    </footer>

</div>


<div id="forgot-password" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-lock"></i> Forgot Password</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'passwordReset']]); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="row col-md-12 center-table">
                        <div class="form-inline">
                            <div class="form-group col-md-12">
                                <p>Enter your email address below and we'll send your new password to your inbox.</p>
                            </div>
                            <div class="form-group col-md-12 top-buffer">
                                <input type="email" id="email" name="email" class="form-control" required="required"
                                       placeholder="Email">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-warning">Reset Password</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<!-- Response Dialog -->
<div id="response-dialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Important js put in all pages -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('general.js'); ?>
</body>
</html>
