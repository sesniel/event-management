<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Sesniel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
	<?= $this->Html->css($this->request->session()->read("theme") . '/bootstrap.min.css') ?>
	<?= $this->Html->css($this->request->session()->read("theme") . '/plugins.css') ?>
	<?= $this->Html->css($this->request->session()->read("theme") . '/opensans-web-font.css') ?>
	<?= $this->Html->css($this->request->session()->read("theme") . '/montserrat-web-font.css') ?>
	<?= $this->Html->css($this->request->session()->read("theme") . '/style.css') ?>
	<?= $this->Html->css($this->request->session()->read("theme") . '/responsive.css') ?>
	<?= $this->Html->css($this->request->session()->read("theme") . '/font-awesome.min.css') ?>
        <?= $this->Html->script($this->request->session()->read("theme") . '/vendor/modernizr-2.8.3-respond-1.4.2.min.js'); ?>
        
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		
		<div class='preloader'><div class='loaded'>&nbsp;</div></div>
        <nav class="mainmenu navbar navbar-default navbar-fixed-top">
            <div class="container">
			
			<div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					
					<div class="brand-bg">
                    <a class="navbar-brand" href="index.html">
                        
                        <?php
                            echo $this->Html->image($this->request->session()->read("theme") . '/logo2.png');
                        ?></a>
					</div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav pull-right">
                        
                        <li><a href="#">About</a></li>
                        <li><a href="#">Work</a></li>
                        <li><a href="#">Portfolio</a></li>
                        <li><a href="#">Contact</a></li>
                        <li>
                            <?php
                                echo $this->Html->link(
                                    'Logout',
                                    array('controller' => '../users', 'action' => 'logout'),
                                    ['escape' => false]
                                );
                            ?>
                        </li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        
                    </ul>
                </div><!-- /.navbar-collapse -->
				</div>
				
            </div><!-- /.container-fluid -->
        </nav>

        <!--Home page style-->
        <header class="home-bg">
		<div class="overlay-img">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="header-details">
							<h1>intelligence<br> is nothing without ambition<i class="fa fa-circle"></i></h1>
							<p>We’re small but impressive creative agency.</p>
							<button class="btn btn-default">Read More</button>
						</div>
					</div>
					

				</div>
			</div>
		</div>	
        </header>

        <!-- Sections -->
        <section id="promotion-area" class="sections">
            <div class="container">
                <!-- Example row of columns -->
                <div class="row">
					<div class="col-sm-4 col-sm-12 col-xs-12">
						<div class="promotion">
						
						<h1>Peace<br> of Mind<i class="fa fa-circle"></i></h1>
						<p>You’re safe with us. The privacy and security of your financial information is our top priority.</p>
						<a href="#"><h4>more<span><i class="fa fa-long-arrow-right"></i></span></h4></a>
						
						</div>
					</div>
					
					<div class="col-sm-4 col-sm-12 col-xs-12">
						<div class="promotion">
						
						<h1>Free<br>Actually free<i class="fa fa-circle"></i></h1>
						<p>You’re safe with us. The privacy and security of your financial information is our top priority.</p>
						<a href="#"><h4>more<span><i class="fa fa-long-arrow-right"></i></span></h4></a>
						
						</div>
					</div>
					
					<div class="col-sm-4 col-sm-12 col-xs-12">
						<div class="promotion">
						
						<h1>Simple<br> payments<i class="fa fa-circle"></i></h1>
						<p>You’re safe with us. The privacy and security of your financial information is our top priority.</p>
						<a href="#"><h4>more<span><i class="fa fa-long-arrow-right"></i></span></h4></a>
						
						</div>
					</div>

                </div>
            </div> <!-- /container -->       
        </section>
		
		
		<section id="portfolio-area" class="sections">
            <div class="container">
                <!-- Example row of columns -->
                <div class="row">
					<div class="col-sm-6 col-sm-12 col-xs-12">
						<div class="portfolio">
                                                        <?php
                                                            echo $this->Html->image($this->request->session()->read("theme") . '/computer.png');
                                                        ?>
						</div>
					</div>
					
					<div class="col-sm-5 col-sm-12 col-xs-12">
						<div class="portfolio">
						
						<div class="item">
						<h1>Portfolio<i class="fa fa-circle"></i></h1>
						<h5>We are small, but strong team</h5>
						<p>The same debit card you use to buy coffee lets deposit money straight to your bank account.</p>
						<p>You’re safe with us. The privacy and security of your financial information is our top priority.</p>
						<a href="#"><h4>more<span><i class="fa fa-long-arrow-right"></i></span></h4></a>
						</div>
						
						</div>
					</div>

                </div>
            </div> <!-- /container -->       
        </section>
		
		
		<section id="our-team" class="sections">
            <div class="container">
                <!-- Example row of columns -->
                <div class="row">
						<div class="team-heading">
							<h1>Our Team<i class="fa fa-circle"></i></h1>
							<p>They are big but still friendly.</p>
						</div>
				
				<div class="team-member">
					
					<div class="col-sm-4 col-sm-12 col-xs-12">
						<div class="team">
                                                        <?php
                                                            echo $this->Html->image($this->request->session()->read("theme") . '/man1.png');
                                                        ?>
							<div class="team-info">
								<h2>Joshua</h2>
								<h5>Web Developer</h5>
								<p>Coffee lover.  Always on bike. iPhone fan.</p>
							</div>
						</div>	
					</div>
					
					<div class="col-sm-4 col-sm-12 col-xs-12">
						<div class="team">
                                                        <?php
                                                            echo $this->Html->image($this->request->session()->read("theme") . '/man2.png');
                                                        ?>
							<div class="team-info">
								<h2>Sesniel</h2>
								<h5>Web Designer</h5>
								<p>Coffee lover.  Always on bike. iPhone fan.</p>
							</div>
						</div>	
					</div>
					
					<div class="col-sm-4 col-sm-12 col-xs-12">
						<div class="team">
                                                        <?php
                                                            echo $this->Html->image($this->request->session()->read("theme") . '/man3.png');
                                                        ?>
							<div class="team-info">
								<h2>Deloso</h2>
								<h5>Copywriter</h5>
								<p>Coffee lover.  Always on bike. iPhone fan.</p>
							</div>
						</div>	
					</div>
					
					<div class="team-more">
						<a href="#"><h4>more<span><i class="fa fa-long-arrow-right"></i></span></h4></a>
					</div>
					
				</div>	
					
				</div>
                
            </div> <!-- /container -->       
        </section>
		
		<div class="scroll-top">
		
			<div class="scrollup">
				<i class="fa fa-angle-double-up"></i>
			</div>
			
		</div>
	
        <!--Footer-->
        <footer>
            <div class="container">
            </div>
        </footer>

        <?= $this->Html->script($this->request->session()->read("theme") . '/vendor/jquery-1.11.2.min.js'); ?>
        <?= $this->Html->script($this->request->session()->read("theme") . '/vendor/bootstrap.min.js'); ?>
        <?= $this->Html->script($this->request->session()->read("theme") . '/main.js'); ?>
        <?= $this->Html->script($this->request->session()->read("theme") . '/plugins.js'); ?>
            

    </body>
</html>
