
<br />
<br />
<div class="row">
    <div class="col-sm-12 col-md-4 col-md-offset-4 sign-in animated flipInY">
        <div id="signup" class="lock-screen" >
            <div class="title icon-space">
                Welcome, as this is your first visit please create your family password below.
            </div>
            <?php
            echo $this->Form->create(null, [
                    'url' => ['controller' => 'users', 'action' => 'password-update']
                ]);
            ?>
                <fieldset>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" required="required"/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password_confirm" placeholder="Confirm Password" required="required"/>
                    </div>
                    <div class="actions">
                            <input tabindex="9" class="btn btn-success large pull-right" type="submit" value="Update Password">
                            <br/>&nbsp;<br/>
                    </div>						            	           					
                </fieldset>
            </form>
        </div>																					
    </div><!-- login container End here -->					
</div>
