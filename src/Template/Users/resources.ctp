<div class="row centered-form">
    <div class="col-sm-12 col-md-12">
            <div class="widget-box">
                    <div class="widget-title">
                            <h2><strong>Resources</strong></h2>
                    </div>
                    <hr class="customHr">
                    <div class="widget-container table-responsive">
                            <div class="row">
                                    <div class="training-article">
                                            <h3>Controlling School Clutter:</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ratione ipsum, necessitatibus maiores suscipit beatae laborum consequuntur ex officia optio non quae fugit! Ven Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae earum accusantium quae fugiat adipisci, quibusdam minima? Sit iure, aliquid iusto nemo cumque, hic, suscipit minima excepturi laudantium sapiente, autem pariatur.iam dignissimos deleniti, similique ipsam non alias.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, dolore.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae quis vero, nesciunt voluptate error hic earum dolorem culpa rem pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, qui voluptates doloribus eius ipsum soluta hic necessitatibus blanditiis earum inventore quas cupiditate iusto, possimus consequatur quisquam vero? Facere architecto, minus.</p>
                                            <h3>5 Ways You Can Keep Your Child On Track This Term</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque omnis eius saepe ad, eligendi quisquam eveniet, necessitatibus fuga corporis sapiente recusandae nihil odio excepturi consequuntur cupiditate! Est soluta mollitia, nemo veritatis quae magni assumenda repudiandae!</p>		
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, laudantium cum minima, odio, quia quasi consequuntur obcaecati ipsam ullam iure natus eos atque cupiditate earum. Amet nam quod fuga eius!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia velit quia alias eveniet, ut quisquam error maiores ratione corporis molestias? Aut beatae doloribus quas, eaque earum? Officiis, laboriosam, repudiandae. Expedita, rem, pariatur? Temporibus facere maxime tenetur illo inventore veritatis excepturi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque omnis eius saepe ad, eligendi quisquam eveniet, necessitatibus fuga corporis sapiente recusandae nihil odio excepturi consequuntur cupiditate! Est soluta mollitia, nemo veritatis quae magni assumenda repudiandae!</p>	
                                            <h3>Helping Your Children With Assignment Preparation</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque omnis eius saepe ad, eligendi quisquam eveniet, necessitatibus fuga corporis sapiente recusandae nihil odio excepturi consequuntur cupiditate! Est soluta mollitia, nemo veritatis quae magni assumenda repudiandae!</p>		
                                            <h3>Student Case Study – Confidence, Motivation &amp; One on One Service – Massive Results</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque omnis eius saepe ad, eligendi quisquam eveniet, necessitatibus fuga corporis sapiente recusandae nihil odio excepturi consequuntur cupiditate! Est soluta mollitia, nemo veritatis quae magni assumenda repudiandae!</p>		
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, laudantium cum minima, odio, quia quasi consequuntur obcaecati ipsam ullam iure natus eos atque cupiditate earum. Amet nam quod fuga eius!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia velit quia alias eveniet, ut quisquam error maiores ratione corporis molestias? Aut beatae doloribus quas, eaque earum? Officiis, laboriosam, repudiandae. Expedita, rem, pariatur? Temporibus facere maxime tenetur illo inventore veritatis excepturi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque omnis eius saepe ad, eligendi quisquam eveniet, necessitatibus fuga corporis sapiente recusandae nihil odio excepturi consequuntur cupiditate! Est soluta mollitia, nemo veritatis quae magni assumenda repudiandae!</p>							
                                    </div>
                            </div>
                    </div><!-- End .widget-container-->
            </div> 
    </div>

    <div class="col-sm-12 col-md-12">
                            <div class="widget-box">
                                    <div class="widget-title">
                                            <h2><strong>List of Training Modules</strong></h2>
                                    </div>
                                    <hr class="customHr">
                                    <div class="widget-container table-responsive">
                                            <div class="row">
                                                    <div class="training-article">
                                                            <h3>Title Here:</h3>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ratione ipsum, necessitatibus maiores suscipit beatae laborum consequuntur ex officia optio non quae fugit! Veniam dignissimos deleniti, similique ipsam non alias.</p>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, dolore.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae quis vero, nesciunt voluptate error hic earum dolorem culpa rem pariatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, qui voluptates doloribus eius ipsum soluta hic necessitatibus blanditiis earum inventore quas cupiditate iusto, possimus consequatur quisquam vero? Facere architecto, minus.</p>
                                                            <h3>Another Title here</h3>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque omnis eius saepe ad, eligendi quisquam eveniet, necessitatibus fuga corporis sapiente recusandae nihil odio excepturi consequuntur cupiditate! Est soluta mollitia, nemo veritatis quae magni assumenda repudiandae!</p>		
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, laudantium cum minima, odio, quia quasi consequuntur obcaecati ipsam ullam iure natus eos atque cupiditate earum. Amet nam quod fuga eius!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia velit quia alias eveniet, ut quisquam error maiores ratione corporis molestias? Aut beatae doloribus quas, eaque earum? Officiis, laboriosam, repudiandae. Expedita, rem, pariatur? Temporibus facere maxime tenetur illo inventore veritatis excepturi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque omnis eius saepe ad, eligendi quisquam eveniet, necessitatibus fuga corporis sapiente recusandae nihil odio excepturi consequuntur cupiditate! Est soluta mollitia, nemo veritatis quae magni assumenda repudiandae!</p>							
                                                    </div>
                                            </div>
                                    </div><!-- End .widget-container-->
                            </div> 
                    </div>
</div>
