<?php

namespace App\Controller;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;

class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
    
    
    public function checkLoginStatus(){

        $this->viewBuilder()->layout($this->request->session()->read("theme"));
        $type = ($this->request->session()->check("type") ? $this->request->session()->read("type") : 11);
        
        $usersTable = TableRegistry::get('Users');
        $userDetails = $usersTable->find('all', ['conditions' => ['Users.token' => $this->request->session()->read("token")]])
            ->select(['id', 'first_name', 'last_name', 'email', 'token'])
            ->first();
        if($userDetails):
            return $userDetails;
        else:
            $this->redirect('/');
        endif;

    }
}
