<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
//use Cake\Mailer\Email;

class UserController extends AppController {

    public function registration(){
    
        $this->checkLoginStatus();
        $userTable  = TableRegistry::get('Users');
        $userDetails= $userTable->find('all', ['order' => ['Users.id' => 'desc']]);
        
        if ($this->request->is('post') || $this->request->is('put')):
            
            $userInfo   = $userTable->newEntity();
            $this->request->data['status'] = "Active";
            $userDetail = $userTable->patchEntity($userInfo, $this->request->data);
            $userDetail->password = crypt($this->request->data['password'],99);
            $userDetail->token = md5( $this->request->data['email'] );
            if($this->request->data['type'] == "admin"):
                $userDetail->theme  = "admin";
            endif;
            if ($userTable->save($userDetail)) {
                
                $this->Flash->success('<div class="alert alert-success alert-bordered">
                                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                                <span class="text-semibold">User Registration Successfull!
                                        </div>',['escape' => false]);
                return $this->redirect(['action' => 'registration']);
            }else{
                $this->Flash->error('<div class="alert alert-danger alert-bordered">
                                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                                    <span class="text-semibold">Something goes wrong on your regsitration, please try again</a>.
                                        </div>',['escape' => false]);
            }
            
        endif;
        
        $this->set(compact('userDetails'));

    }
    
}
