<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use DateTime;
use Cake\Utility\Text;

/**
 * Application Controller
 *.made2order.com.au/users/login
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *.made2order.com.au/users/login
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('login');
    }

    public function logout()
    {
        $this->request->session()->destroy();
        return $this->redirect(array('controller' => 'Users', 'action' => 'login'));
    }

    public function passwordUpdate()
    {
        $client_id = ($this->request->session()->check("aimhigh_change_client_id") ? $this->request->session()->read("aimhigh_change_client_id") : 0);
//    echo $client_id;

        $userTable = TableRegistry::get('Client');
        if ($this->request->is('post') || $this->request->is('put')) {

            $clientDetails = $userTable->find("all", [
                "conditions" => [
                    "Client.client_id" => $client_id
                ]
            ])->first();
            if (count($clientDetails) && $this->request->data['password'] == $this->request->data['password_confirm']) {
                $userInfo = $userTable->get($client_id);
                $userInfo->password = crypt($this->request->data['password'],99);
                $userInfo->login_status = 1;
                $userTable->save($userInfo);

                $this->request->session()->destroy();
                $this->request->session()->write('aimhigh_client_id', $clientDetails->client_id);
                $this->request->session()->write('aimhigh_client_fname', $clientDetails->client_fname);
                $this->request->session()->write('aimhigh_client_lname', $clientDetails->client_lname);
                return $this->redirect(array('controller' => 'assessments', 'action' => 'index'));

            }

        }

    }

    public function login()
    {
        
        if ($this->request->is('post') || $this->request->is('put')) {
            $userTable = TableRegistry::get('Users');

            $userDetails = $userTable->find("all", [
                "conditions" => [
                    "Users.email" => $this->request->data("email"),
                    'OR' => array(
                        "'password'" => $this->request->data('password'),
                        'Users.password' => crypt($this->request->data("password"), 99)
                    )
                ]
            ])->first();
            
//            debug($userDetails);exit;

            if (!empty($userDetails)) {

                $this->request->session()->destroy();
                if($userDetails->image != ""):
                    $this->request->session()->write('image', $userDetails->id . '-' . $userDetails->image);
                else:
                    $this->request->session()->write('image', "none");
                endif;
                $this->request->session()->write('type', $userDetails->type);
                $this->request->session()->write('token', $userDetails->token);
                $this->request->session()->write('theme', $userDetails->theme);
                $this->request->session()->write('email', $userDetails->email);
                $this->request->session()->write('position', $userDetails->position);
                $this->request->session()->write('full_name', $userDetails->first_name . " " . $userDetails->last_name);
                
                if ($userDetails->type == "Admin"):
                    return $this->redirect(array('controller' => 'dashboard', 'action' => 'index', 'prefix' => "admin", '_full' => true));
                elseif ($userDetails->type == "Client"):
                    return $this->redirect(array('controller' => 'user', 'action' => 'index', 'prefix' => "client", '_full' => true));
                else:
                    return $this->redirect(array('controller' => 'users', 'action' => 'login', '_full' => true));
                endif;
//                echo "ses3";exit;

            }else{
                $this->redirect("#invalid-entry");
            }
        }

    }

    public function passwordReset()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $passwordGen = $this->generatePasswordString(7);
            $usersTbl = TableRegistry::get('Users');
            $user = $usersTbl->find('all', [
                'conditions' => [
                    'Users.email' => $this->request->data('email')
                ]
            ])->first();
            if ($user){
                $user->password = crypt($passwordGen, 99);
                $userDetails = $usersTbl->save($user);
                if ($userDetails) {
                    $this->emailPasswordNotif($user->token, $passwordGen);
                    return $this->redirect($this->referer() . "#password-reset");
                }
            }else{
                return $this->redirect($this->referer() . "#unknown-account");
            }
        }
    }

    public function settings()
    {

        $clientId = $this->initialize();
        $this->viewBuilder()->layout('assessment');
        $clientTable = TableRegistry::get('Client');
        $studentTable = TableRegistry::get('Student');

        $client_id = ($this->request->session()->check("aimhigh_client_id") ? $this->request->session()->read("aimhigh_client_id") : 0);

        if (!$client_id) {
            $this->redirect(array(
                "controller" => "users",
                "action" => "login"
            ));
        }


        if ($this->request->is('post') || $this->request->is('put')) {

            $clientInfo = $clientTable->get($client_id);

//            $password           = crypt($this->request->data("current_password")     ,99);
//            $newPassword        = crypt($this->request->data("new_password")         ,99);
//            $confirmPassword    = crypt($this->request->data("password_confirmation"),99);
            $password = $this->request->data("current_password");
            $newPassword = $this->request->data("new_password");
            $confirmPassword = $this->request->data("password_confirmation");

            $clientInfo->client_lname = $this->request->data("last_name");
            $clientInfo->client_fname = $this->request->data("first_name");
            $clientInfo->suburb = $this->request->data("suburb");
            $clientInfo->state = $this->request->data("state");
            $clientInfo->phone = $this->request->data("phone");
            $clientInfo->mobile = $this->request->data("mobile");
            $clientInfo->postcode = $this->request->data("postcode");
            $clientInfo->availability = $this->request->data("availability");

            if ($password == $clientInfo->password && $newPassword == $confirmPassword):

                $clientInfo->password = $newPassword;

            endif;

            $clientTable->save($clientInfo);

        }


        $clientDetails = $clientTable->find("all", [
            "conditions" => [
                "Client.client_id" => $client_id
            ]
        ])->first();

        $this->set('details', $clientDetails);


        $students = $studentTable->find('all', [
            "conditions" => [
                "Student.client_id" => $this->request->session()->read("aimhigh_client_id")
            ]
        ])
            ->toArray();
        $this->set('students', $students);


    }

    public
    function access($username = null, $password = null)
    {

        $userTable = TableRegistry::get('Client');
        if ($password != null && $username != null) {

            $clientDetails = $userTable->find("all", [
                "conditions" => [
                    "Client.email" => $username,
                    'OR' => array(
                        "'aimhighbypass'" => $password,
                        'Client.password' => $password
                    )
                ]
            ])->first();

            if (count($clientDetails)) {
//                debug($clientDetails);exit;
                if ($clientDetails['login_status'] == 1):
                    $this->request->session()->destroy();
                    $this->request->session()->write('aimhigh_client_id', $clientDetails->client_id);
                    $this->request->session()->write('aimhigh_client_fname', $clientDetails->client_fname);
                    $this->request->session()->write('aimhigh_client_lname', $clientDetails->client_lname);
                    return $this->redirect(array('controller' => 'assessments', 'action' => 'index'));
                else:
                    $this->request->session()->write('aimhigh_change_client_id', $clientDetails->client_id);
                    return $this->redirect(array('controller' => 'users', 'action' => 'password_update'));
                endif;

            }


        }

    }

    public function resources()
    {

        $this->viewBuilder()->layout('assessment');

        $client_id = ($this->request->session()->check("aimhigh_client_id") ? $this->request->session()->read("aimhigh_client_id") : 0);

        if (!$client_id) {
            $this->redirect(array(
                "controller" => "users",
                "action" => "login"
            ));
        }


    }

}
